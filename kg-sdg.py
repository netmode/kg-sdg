from neo4j import GraphDatabase
import requests

class HelloWorldExample:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        self.driver.close()

    def print_greeting_old(self, message):
        with self.driver.session() as session:
            greeting = session.write_transaction(self._create_and_return_greeting, message)
            print(greeting)

    @staticmethod
    def _create_and_return_greeting(tx, message):
        result = tx.run("CREATE (a:Greeting) "
                        "SET a.message = $message "
                        "RETURN a.message + ', from node ' + id(a)", message=message)
        return result.single()[0]
    
    def print_greeting(self, url):
        with self.driver.session() as session:
            urls = session.write_transaction(self._retrieve_articles)
            base_url = 'http://linkedsdg.apps.officialstatistics.org/swaggerapi/url?url='
            for url in urls:
                article_url = url
                url = url.replace(':','%3A').replace('/','%2F')
                url =base_url+url+'&text=false&geoAreas=true&concepts=false&sdgs=true'
                print(url)
                #https%3A%2F%2Fvariety.com%2F2021%2Ftv%2Fnews%2Fmgm-amazon-mark-burnett-survivor-1234982257%2F&text=false&geoAreas=true&concepts=false&sdgs=true'
                greeting = session.write_transaction(self._consume_linkedsdg_api, article_url, url)
                print(greeting)
            
    @staticmethod
    def _retrieve_articles(tx):
        result = tx.run("MATCH (a:Article)"
                        "RETURN a.url AS url")
        urls = [record["url"] for record in result]
        return urls;
        
    
    @staticmethod
    def _consume_linkedsdg_api(tx, article_url, url):
        url = url
        myobj = {'text': False, 'geoAreas': True, 'concepts': False, 'sdgs': True}
        #x = requests.post(url, data = myobj)
        #print(x.text)
        #x = requests.post('http://linkedsdg.apps.officialstatistics.org/swaggerapi/url?url=https%3A%2F%2Fvariety.com%2F2021%2Ftv%2Fnews%2Fmgm-amazon-mark-burnett-survivor-1234982257%2F&text=false&geoAreas=true&concepts=false&sdgs=true')
        x = requests.post(url)
        #print(x.text)
        #print(x.json())
        y = x.json()
        print(y['geoAreas'])
        geoAreas = y['geoAreas']
        for area in geoAreas:
            print(area['label'])
            result = tx.run("MERGE (a:geoArea{label:$message}) "
                "RETURN a", message=area['label'])
            print(result.single()[0])
            result = tx.run("MATCH (a:Article {url:$message}), (g:geoArea{label:$area})"
                           "MERGE (a)-[r:IN_AREA]->(g) "
                           "RETURN a,g", message=article_url, area =area['label'])
            #print(result.single()[0])
        sdgs = y['sdgs']['children']
        for sdg in sdgs:
            print(sdg['code'])
            result = tx.run("MATCH (a:Article {url:$message}), (g:goal{code:$code})"
                           "MERGE (a)-[r:ABOUT]->(g) "
                           "RETURN a,g", message=article_url, code =sdg['code'])
            #print(result.single()[0])
            
            

if __name__ == "__main__":
    greeter = HelloWorldExample("bolt://localhost:7687", "neo4j", "password")
    #greeter.print_greeting_old("hello, world")
    #greeter.close()
    greeter.print_greeting('https://variety.com/2021/tv/news/mgm-amazon-mark-burnett-survivor-1234982257/')
    greeter.close()
