This is a repository that contains the population of a Knowledge Graph where world wide news are automatically retrieved and interlinked with wikipages and enriched with metadata regarding the SDGs and the countries they refer to.
Queries upon the KG-SDG facilite the retrieval of news articles per SDG and per country & SDG.

## Prerequisites

Install Neo4j Community Edition:

```
sudo apt-get install neo4j
```

Install [neosemantics (n10s)](https://neo4j.com/labs/neosemantics/4.1/introduction/) plugin.  neosemantics plugin enables the use of RDF and its associated vocabularies like OWL, RDFS, SKOS, and others in Neo4j.

Install [APOC](https://neo4j.com/labs/apoc/) (Awesome Procedures on Neo4j) plugin.  APOC (Awesome Procedures on Neo4j) contains more than 450 procedures and functions providing functionality for utilities, conversions, graph updates, and more. We’re going to use this tool to scrape web pages and apply NLP techniques on text data.

Install python package of neo4j
```
pip install neo4j
```

## Retrieve fresh News

Start neo4j service
```
sudo neo4j start
```
Following scrip automatically retrieves worldwide news and links them to the relevand SDG and geoAreas
```
python3 kg-sdg.py
```
